## 仓库简介

对象存储服务（Object Storage Service,OBS）是一个基于对象的存储服务，为客户提供海量、安全、高可靠、低成本的数据存储能力，使用时无需考虑容量限制，并且提供多种存储类型供选择，满足客户各类业务场景诉求，支持数据的上传下载，网站托管，日志采集监控等功能。

#  项目总览

<table style="text-align: center">
    <tr style="font-weight: bold">
        <td>项目</td>
        <td>介绍</td>
        <td>仓库</td>
    </tr>  
    <tr>
        <td rowspan="1">语音识别</td>
        <td>自动将用户上传的wav语音文件转化为文字，并存放到指定OBS桶</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-speech-recognition">huaweicloud-solution-speech-recognition</a></td>
    </tr>
    <tr>
        <td rowspan="1">构建私有网盘</td>
        <td>基于Zpan快速构建私有网盘</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-zpan-based-private-network-disks">huaweicloud-solution-zpan-based-private-network-disks</a></td>
    </tr>
     <tr>
        <td rowspan="1">静态网站托管</td>
        <td>OBS与CMS深度整合，提供一个无技术门槛、可快速搭建100%自由度的网站</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms">huaweicloud-obs-website-wangmarketCMS</a></td>
    </tr>
     <tr>
        <td rowspan="2">接口调用示例</td>
        <td>OBS接口调用示例</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-SDK-samples">huaweicloud-obs-SDK-samples</a></td>
    </tr>
    <tr>
        <td>体验使用CAE serverless化托管运维后端应用</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cae-backend">huaweicloud-cae-backend</a></td>
      </tr>
    <tr>
        <td rowspan="4">数据上传下载</td>
        <td>参考样例对OBS进行桶操作和对象操作</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-helper-workflow-sampler">obs-helper-workflow-sample</a></td>
    </tr> 
    <tr>
        <td>参考样例对OBS进行上传、下载文件和桶操作</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-helper">obs-helper</a></td>
      </tr> 
    <tr>
        <td rowspan="2">以表单形式上传数据至华为云OBS</td>
    	<td><a href="https://gitee.com/HuaweiCloudDeveloper/obsform-upload-demo-frontend">OBSFormUploadDemo-frontend</a></td>
    </tr>
    <tr>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/OBSFormUploadDemo-backend">OBSFormUploadDemo-backend</a></td>		
    </tr> <tr>
        <td rowspan="3">数据工坊</td>
        <td>发布三方算子并提交审核</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dwr-sdk-demo-action-go">huaweicloud-dwr-sdk-demo-action-go</a></td>
    </tr> 
    <tr>
        <td>创建工作流并获取工作流的执行结果</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dwr-sdk-demo-custom-go">huaweicloud-dwr-sdk-demo-custom-go</a></td>
      </tr> 
    <tr>
        <td>创建图片裁剪工作流并对OBS桶里的图片进行裁剪</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dwr-sdk-demo-img-crop-go"> huaweicloud-dwr-sdk-demo-img-crop-go</a></td>
    </tr>  
    <tr>
        <td rowspan="1">集成OBS和RDS</td>
        <td>实现用户登录、图片上传、更新等功能，并在云端部署日志采集监控等内容</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/dtse-practice">dtse-practice</a></td>
    </tr> 
      <tr>
        <td rowspan="1">clickhouse对接OBS插件</td>
        <td>clickhouse对接华为云OBS插件</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-clickhouse-plugins">obs-clickhouse-plugins</a></td>
    </tr> 
      <tr>
        <td rowspan="1">datax对接OBS插件</td>
        <td>datax对接华为云OBS插件</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins">obs-datax-plugins</a></td>
    </tr> 
      <tr>
        <td rowspan="1">Druid对接OBS插件</td>
        <td>Druid对接OBS插件</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-druid-plugins">obs-druid-plugins</a></td>
    </tr> 
      <tr>
        <td rowspan="1">flink对接OBS插件</td>
        <td>flink对接华为云OBS插件</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-flink-plugins">obs-flink-plugins</a></td>
    </tr> 
      <tr>
        <td rowspan="1">jenkins对接OBS插件</td>
        <td>jenkins对接华为云OBS插件</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-jenkins-plugins">obs-jenkins-plugins</a></td>
    </tr> 
    <tr>
        <td rowspan="1">logstash对接OBS插件</td>
        <td>logstash对接华为云OBS插件</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-logstash-plugins">obs-logstash-plugins</a></td>
    </tr> 
    <tr>
        <td rowspan="1">presto对接OBS插件</td>
        <td>presto对接华为云OBS插件</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-presto-plugin">obs-presto-plugin</a></td>
    </tr>  
    <tr>
        <td rowspan="1">tensorflow对接OBS插件</td>
        <td>tensorflow对接华为云OBS插件</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-tensorflow-plugins">obs-tensorflow-plugins</a></td>
    </tr>
     <tr>
        <td rowspan="1">thanos对接OBS插件</td>
        <td>thanos对接华为云OBS插件</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/obs-thanos-plugins">obs-thanos-plugins</a></td>
    </tr> 
            </td>
</tr>
</table>





​        







